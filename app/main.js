const win = nw.Window.get();
const notifier = require('node-notifier');
const path = require('path');
const axios = require('axios');

function initialize() {
    const menu = new nw.Menu();
    const icon = path.resolve(path.dirname(process.execPath), "icon.png");
    menu.append(new nw.MenuItem({ label: 'Exit', click() { win.close(true) } }));
    const tray = new nw.Tray({
        title: "Test title",
        icon,
        menu
    });
    tray.on('click', function (evt) {
        win.show();
    });
    win.hide();
    win.on('minimize', win.hide);
    win.on('close', win.hide);
    notifier.on('click', function (notifierObject, options, event) {
        win.show();
    });
}

function notify(message) {
    const icon = path.resolve(path.dirname(process.execPath), "icon.png");
    notifier.notify({
        appID: 'customize-checker',
        title: '¯\\_(ツ)_/¯',
        message,
        icon,
        wait: true
    });
}

const checkers = [
    {
        name: 'iframe-layout',
        execute(callback) {
            axios.get('https://dev.navitee.com/iframe/dist/?product=test')
            .then(res => {
                if(res && res.status < 400) {
                    callback(true);
                }
                else {
                    callback(false);
                }
            })
            .catch(e => {
                callback(false);
            })
        }
    },
    {
        name: 'iframe-data',
        execute(callback) {
            axios.get('https://dev.navitee.com/design-upload-v2/products/test/data.json')
            .then(res => {
                if(res && res.status < 400) {
                    callback(true);
                }
                else {
                    callback(false);
                }
            })
            .catch(e => {
                callback(false);
            })
        }
    },
    {
        name: 'manager',
        execute(callback) {
            axios.get('http://dev.navitee.com:3000/design-upload-v2/')
            .then(res => {
                if(res && res.status < 400) {
                    callback(true);
                }
                else {
                    callback(false);
                }
            })
            .catch(e => {
                callback(false);
            })
        }
    }
];

(function main() {
    initialize();

    try {
        const container = document.querySelector('.container');
        let nextCheck = + new Date;
        let checkVersion = 0;
        for(const [checkerID, checker] of checkers.entries()) {
            container.insertAdjacentHTML('beforeend', `
                <div class="checker-row row">
                    <div class="col">${checker.name}</div>
                    <div class="col">
                        <span class="status-dot dot bg-green"></span>
                        <span>&nbsp;</span>
                        <span class="status">Alive</span>
                        <input type="checkbox" class="got-it-checkbox"> I received the notification
                    </div>
                </div>
            `);
            const checkerRow = () => container.querySelectorAll('.checker-row')[checkerID];
            const statusDot = () => checkerRow().querySelector('.status-dot');
            const statusText = () => checkerRow().querySelector('.status');
            const gotItCheckbox = () => checkerRow().querySelector('.got-it-checkbox');
            let localCheckVersion = null;
            setInterval(() => {
                console.log(checkerRow());
                if(+new Date < nextCheck || localCheckVersion == checkVersion) return;
                localCheckVersion = checkVersion;
                statusDot().classList.remove('bg-red');
                statusDot().classList.remove('bg-green');
                statusDot().classList.add('bg-yellow');
                statusText().innerText = 'Checking';
                checker.execute(status => {
                    if(status) {
                        statusDot().classList.remove('bg-red');
                        statusDot().classList.remove('bg-yellow');
                        statusDot().classList.add('bg-green');
                        statusText().innerText = 'Alive';
                        gotItCheckbox().checked = false;
                    }
                    else {
                        statusDot().classList.remove('bg-green');
                        statusDot().classList.remove('bg-yellow');
                        statusDot().classList.add('bg-red');
                        statusText().innerText = 'Error';
                        if(!gotItCheckbox().checked) {
                            notify(`Down service detected: "${checker.name}". Please check.`);
                        }
                    }
                });
            }, 1000);
        }
        function scheduleAutoCheck() {
            if(scheduleAutoCheck.intervalID != undefined) {
                clearInterval(scheduleAutoCheck.intervalID);
            }
            scheduleAutoCheck.intervalID = setInterval(() => {
                nextCheck = +new Date;
                checkVersion++;
            }, 60000);
        }
        container.insertAdjacentHTML('beforeend', `
            <div class="row">
                <div class="col">Actions</div>
                <div class="col">
                    <button id="check-btn">Check</button>
                </div>
            </div>
        `);
        scheduleAutoCheck();
        document.querySelector('#check-btn').onclick = function() {
            nextCheck = +new Date;
            checkVersion++;
            scheduleAutoCheck();
        }
        setInterval(() => {
            const nextCheckRemaining = nextCheck + 60000 - (+new Date);
            if(nextCheckRemaining >= 0) {
                const nrSeconds = Math.floor(nextCheckRemaining / 1000);
                document.querySelector('#auto-check-timer').innerText = `${nrSeconds}s`;
            }
        }, 500);
    }
    catch(e) {
        notify(`Error: ${e}`);
    }

    notify('Customize checker started.');
})();
